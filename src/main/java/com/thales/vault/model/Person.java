package com.thales.vault.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;

/**
 * Simple JavaBean domain object representing an person.
 */
@MappedSuperclass
public class Person extends BaseEntity {

	private static final long serialVersionUID = -1064720283671216241L;

	@Column(name = "name")
	@NotEmpty
	private String name;

	@Column(name = "email")
	@NotEmpty
	private String email;
	
	@Column(name = "office")
	@NotEmpty
	private String office;
	
	@Column(name = "team")
	@NotEmpty
	private String team;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
}
