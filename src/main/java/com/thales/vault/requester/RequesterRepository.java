package com.thales.vault.requester;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository class for <code>Vet</code> domain objects All method names are compliant
 * with Spring Data naming conventions so this interface can easily be extended for Spring
 * Data. See:
 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation
 */
public interface RequesterRepository extends JpaRepository<Requester, Integer> {
//	  @Transactional(readOnly = true)
//	  @Cacheable("requesters")
//	  List<Requester> findByEmail(String email);
}
