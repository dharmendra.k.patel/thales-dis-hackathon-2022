package com.thales.vault.requester;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
class RequesterController {

	@Autowired
	private RequesterRepository requesterRepo;

	@GetMapping("/register")
	public String showRegistrationForm(Model model) {
		model.addAttribute("requester", new Requester()); 
		return "register";
	}
	
	@PostMapping("/provisioning-request")
	public String processProvisioningRequestForm(@Valid Requester requester, BindingResult result) {
		if (result.hasErrors()) {
			return "/error";
		}
		else {
			this.requesterRepo.save(requester);
			return "redirect:/";
		}
	}
	
//	@GetMapping("/submit_request.html")
//	public String showVetList(@RequestParam(defaultValue = "1") int page, Model model) {
//		// Here we are returning an object of type 'Vets' rather than a collection of Vet
//		// objects so it is simpler for Object-Xml mapping
//		Vets vets = new Vets();
//		Page<Vet> paginated = findPaginated(page);
//		vets.getVetList().addAll(paginated.toList());
//		return addPaginationModel(page, paginated, model);
//
//	}
//
//	private String addPaginationModel(int page, Page<Vet> paginated, Model model) {
//		List<Vet> listVets = paginated.getContent();
//		model.addAttribute("currentPage", page);
//		model.addAttribute("totalPages", paginated.getTotalPages());
//		model.addAttribute("totalItems", paginated.getTotalElements());
//		model.addAttribute("listVets", listVets);
//		return "vets/vetList";
//	}
//
//	private Page<Vet> findPaginated(int page) {
//		int pageSize = 5;
//		Pageable pageable = PageRequest.of(page - 1, pageSize);
//		return vets.findAll(pageable);
//	}
//
//	@GetMapping({ "/vets" })
//	public @ResponseBody Vets showResourcesVetList() {
//		// Here we are returning an object of type 'Vets' rather than a collection of Vet
//		// objects so it is simpler for JSon/Object mapping
//		Vets vets = new Vets();
//		vets.getVetList().addAll(this.vets.findAll());
//		return vets;
//	}

}
